using Newtonsoft.Json;

namespace TuyaIoT.DTOs.Tuya;

public class TuyaDeviceStatus
{
    [JsonProperty("code")] public string Code { get; set; }

    [JsonProperty("value")] public object Value { get; set; }
}