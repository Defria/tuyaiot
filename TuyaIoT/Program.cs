// See https://aka.ms/new-console-template for more information

using Newtonsoft.Json;
using TuyaIoT.DTOs.Tuya;
using TuyaIoT.Model.Tuya;
using TuyaIoT.Services.Tuya;

namespace TuyaIoT;

internal static class Program
{
    private static void Main()
    {
        var deviceId = "xxxxxxxxedc57518dbxxxxxxx";
        var tuyaCommand = new Tuya
        {
            Commands = new List<Command>
            {
                new()
                {
                    Code = "switch_led",
                    Value = true
                },
                new()
                {
                    Code = "colour_data_v2",
                    Value = new
                    {
                        h = 357,
                        s = 1000,
                        v = 99
                    }
                }
            }
        };
        var body = JsonConvert.SerializeObject(tuyaCommand);
        var service = new TuyaService();
        var devices = service.GetAllDevicesByUserId(deviceId).Result;
        var studyLights =
            devices.Where(p => p.Name.StartsWith("Study Light", StringComparison.InvariantCultureIgnoreCase));
        foreach (var light in studyLights)
        {
            service.SendCommand(light.Id, body);
            var lightInfo = service.GetDeviceInfoAsync(light.Id).Result;
            Console.WriteLine($"{nameof(lightInfo.Name)}:{lightInfo.Name} - Code:{lightInfo.Status[0].Value} - {nameof(lightInfo.Online)}:{lightInfo.Online}");
        }
    }
}

