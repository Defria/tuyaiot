using Newtonsoft.Json;

namespace TuyaIoT.Model.Tuya;

internal class TuyaToken
{
    [JsonProperty("access_token")] public string AccessToken { get; set; }

    [JsonProperty("expire_time")] public int ExpireTime { get; set; }

    [JsonProperty("refresh_token")] public string RefreshToken { get; set; }

    [JsonProperty("uid")] public string Uid { get; set; }
}

internal class Root<T>
{
    [JsonProperty("result")] public T? Result { get; set; }

    [JsonProperty("success")] public bool Success { get; set; }

    [JsonProperty("t")] public long TimeStamp { get; set; }

    [JsonProperty("tid")] public string Tid { get; set; }
}