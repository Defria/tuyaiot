using Newtonsoft.Json;

namespace TuyaIoT.Model.Tuya;

public class Command
{
    [JsonProperty("code")] public string Code { get; set; }
    [JsonProperty("value")] public object Value { get; set; }
}