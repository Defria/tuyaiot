﻿using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using Microsoft.Extensions.Configuration;

namespace TuyaIoT.Configuration;

public class TuyaAPIConfiguration
{
    private const string AppSettings = "appsettings.json";
    private readonly IConfigurationRoot _config;

    public TuyaAPIConfiguration()
    {
        if (!File.Exists(@"appsettings.json"))
            throw new FileNotFoundException($"Configuration file {AppSettings}");

        var builder = new ConfigurationBuilder()
            .AddUserSecrets(Assembly.GetExecutingAssembly(), true)
            .AddJsonFile(AppSettings, true, true)
            .AddEnvironmentVariables();
        _config = builder.Build();
    }

    public string BaseUrl => "https://openapi.tuyaeu.com";
    public string ClientId => _config.GetValue<string>("Tuya:clientId");
    public string ClientSecret => _config.GetValue<string>("Tuya:clientSecret");
    public string Timestamp => GenerateTuyaTimeStamp();
    public string Sign => CalculateTuyaSign(ClientId, ClientSecret, Timestamp);

    private static string GenerateTuyaTimeStamp()
    {
        return (DateTime.Now.ToUniversalTime() - new DateTime(1970, 1, 1)).TotalMilliseconds.ToString("0");
    }

    private static string CalculateTuyaSign(string clientId, string accessToken, string timestamp)
    {
        var stringToSign = clientId + accessToken + timestamp;
        var stringToSignInBytes = Encoding.UTF8.GetBytes(stringToSign);
        var accessTokenInBytes = Encoding.UTF8.GetBytes(accessToken);

        using var hash = new HMACSHA256(accessTokenInBytes);
        var computedHash = hash.ComputeHash(stringToSignInBytes);

        return BitConverter.ToString(computedHash).Replace("-", "").ToUpper();
    }
}