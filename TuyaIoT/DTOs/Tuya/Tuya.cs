using Newtonsoft.Json;
using TuyaIoT.Model.Tuya;

namespace TuyaIoT.DTOs.Tuya;

public class Tuya
{
    [JsonProperty("commands")] public List<Command> Commands { get; set; }
}