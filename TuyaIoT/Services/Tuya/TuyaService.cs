using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using TuyaIoT.Configuration;
using TuyaIoT.DTOs.Tuya;
using TuyaIoT.Model.Tuya;

namespace TuyaIoT.Services.Tuya;

public class TuyaService
{
    private readonly TuyaAPIConfiguration _config = new();
    private readonly HttpClient _httpClient;
    private TuyaToken? _token;
    private DateTime _tokenTime;

    public TuyaService()
    {
        _httpClient = new HttpClient();
    }

    private async Task<string> RequestAsync(Method method, string uri, string? body = null,
        Dictionary<string, string>? headers = null, bool noAccessToken = false, bool forceTokenRefresh = false,
        CancellationToken cancellationToken = default)
    {
        if (uri.StartsWith("/")) uri = uri[1..];
        var url = new Uri($"{_config.BaseUrl}/{uri}");
        var timeStamp = (DateTime.Now.ToUniversalTime() - new DateTime(1970, 1, 1)).TotalMilliseconds.ToString("0");
        var headersConcat = string.Empty;
        if (headers == null)
        {
            headers = new Dictionary<string, string>();
        }
        else
        {
            headersConcat = string.Concat(headers.Select(kv => $"{kv.Key}:{kv.Value}\n"));
            headers.Add("Signature-Headers", string.Join(":", headers.Keys));
        }

        var payload = _config.ClientId;
        switch (noAccessToken)
        {
            case true:
                payload += timeStamp;
                headers["secret"] = _config.ClientSecret;
                break;
            case false:
                var token = await RefreshAccessTokenAsync(forceTokenRefresh, cancellationToken);
                payload += token.AccessToken + timeStamp;
                headers["access_token"] = token.AccessToken;
                break;
        }


        using (var sha256 = SHA256.Create())
        {
            payload += $"{method.ToString().ToUpper()}\n" +
                       string.Concat(sha256.ComputeHash(Encoding.UTF8.GetBytes(body ?? "")).Select(b => $"{b:x2}")) +
                       '\n' +
                       headersConcat + '\n' +
                       url.PathAndQuery;
        }


        string signature;
        using (var hmac = new HMACSHA256(Encoding.UTF8.GetBytes(_config.ClientSecret)))
        {
            signature = string.Concat(hmac.ComputeHash(Encoding.UTF8.GetBytes(payload)).Select(b => $"{b:X2}"));
        }

        headers["client_id"] = _config.ClientId;
        headers["sign"] = signature;
        headers["t"] = timeStamp;
        headers["sign_method"] = "HMAC-SHA256";


        var httpRequestMessage = new HttpRequestMessage
        {
            Method = method switch
            {
                Method.Get => HttpMethod.Get,
                Method.Post => HttpMethod.Post,
                Method.Put => HttpMethod.Put,
                Method.Delete => HttpMethod.Delete,
                _ => throw new ArgumentOutOfRangeException(nameof(method), method, "Unknown method request.")
            },
            RequestUri = url
        };

        foreach (var h in headers)
            httpRequestMessage.Headers.Add(h.Key, h.Value);
        if (body is not null)
            httpRequestMessage.Content = new StringContent(body, Encoding.UTF8, "application/json");

        using var response = await _httpClient.SendAsync(httpRequestMessage, cancellationToken).ConfigureAwait(false);
        var responseString = await response.Content.ReadAsStringAsync(cancellationToken).ConfigureAwait(false);
        var root = JObject.Parse(responseString);
        var success = root.GetValue("success").Value<bool>();
        if (!success)
            throw new InvalidDataException(root.ContainsKey("msg") ? root.GetValue("msg").Value<string>() : null);
        var result = root.GetValue("result").ToString();
        return responseString;
    }

    private async Task<TuyaToken> RefreshAccessTokenAsync(bool force = false,
        CancellationToken cancellationToken = default)
    {
        if (force || _token == null || _tokenTime.AddSeconds(_token.ExpireTime) >= DateTime.Now ||
            _tokenTime.AddMinutes(30) >= DateTime.Now)
        {
            const string uri = "v1.1/token?grant_type=1";
            var response =
                await RequestAsync(Method.Get, uri, noAccessToken: true, cancellationToken: cancellationToken);
            var tokenResponse = JsonConvert.DeserializeObject<Root<TuyaToken>>(response);
            _token = tokenResponse?.Result;
            _tokenTime = DateTime.Now;
        }

        return _token ?? new TuyaToken();
    }

    public void SendCommand(string deviceId, string jsonBody)
    {
        var api = new TuyaService();
        var response = api.RequestAsync(Method.Post, $"v1.0/iot-03/devices/{deviceId}/commands", jsonBody).Result;
        Console.WriteLine(response);
    }

    public async Task<TuyaDeviceApiInfo> GetDeviceInfoAsync(string deviceId, bool forceTokenRefresh = false,
        CancellationToken cancellationToken = default)
    {
        var uri = $"v1.0/devices/{deviceId}";
        var response = await RequestAsync(Method.Get, uri, forceTokenRefresh: forceTokenRefresh,
            cancellationToken: cancellationToken);
        var device = JsonConvert.DeserializeObject<Root<TuyaDeviceApiInfo>>(response);
        return device.Result;
    }
    
    public async Task<ICollection<TuyaDeviceApiInfo>> GetAllDevicesByUserId(string anyDeviceId, bool forceTokenRefresh = false, CancellationToken cancellationToken = default)
    {
        var userId = (await GetDeviceInfoAsync(anyDeviceId, forceTokenRefresh: forceTokenRefresh, cancellationToken: cancellationToken)).UserId;
        var uri = $"v1.0/users/{userId}/devices";
        var response = await RequestAsync(Method.Get, uri, forceTokenRefresh: false, cancellationToken: cancellationToken); // Token already refreshed
        var devices = JsonConvert.DeserializeObject<Root<ICollection<TuyaDeviceApiInfo>>>(response);
        return devices?.Result;
    }
}